# Fedora Event Handler

Responsible for fetching events coming from Fedora, which indicate a change in the hosted data. The events are filtered
by the handler for by relevancy (see below) and subsequently sent to Kafka (i.e. the handler acts as a Kafka producer). 

## Relevant events

Not every Fedora event is equally important to the services downstream. Because of the distributed data model implemented
by our Fedora instance, several events happen at the same time when a record is newly imported, changed or deleted.
Therefore, it is advisable to "bundle" them. Eventually three types of events are essential:

* Events indicating a created/changed/removed institution
* Events indicating a created/changed/removed collection (a `RecordSet`)
* Events indicating a created/changed/removed record

## Configuration

As any Memobase service communicating with Kafka, `Fedora Event Handler` is eventually deployed to the Kubernetes cluster.
Application settings can be set via environment variables:

* `ACTIVEMQ_HOST`: Host name of Fedora
* `ACTIVEMQ_PORT`: Port over which Fedora sends ActiveMQ messages (`61616` per default)
* `ACTIVEMQ_PUBLISHING_MODE`: How the client [communicates with Fedora](https://activemq.apache.org/how-does-a-queue-compare-to-a-topic). Must be `queue` or `topic`.
* `ACTIVEMQ_QUEUE_OR_TOPIC_NAME`: Name of queue or topic used by Fedora for publishing messages
* `FEDORA_BASE_URL`: Base URL for objects internally used by Fedora
* `EXTERNAL_BASE_URL`: Base URL for objects externally used
* `KAFKA_BOOTSTRAP_SERVERS`: Comma-separated list of `<host>:<port>` tuples pointing to the brokers of the Kafka cluster 
* `CLIENT_ID`: Id of service as seen by Kafka
* `TOPIC_OUT`: Kafka topic in which the transformed events are written 
* `TOPIC_PROCESS`: Can be ignored
