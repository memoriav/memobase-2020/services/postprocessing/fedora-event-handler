/*
 * Fedora Event Handler
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import org.scalatest.funsuite.AnyFunSuite
import upickle.core.AbortException

import scala.io.Source

class ActiveMQMsgSpec extends AnyFunSuite {

  private val fedoraBaseUrl = "http://localhost:8080/fcrepo/rest"
  private val externalBaseUrl = "https://memobase.ch"

  private def loadFileAsString(filename: String): String = {
    val is = Source.fromFile(s"src/test/resources/$filename")
    is.mkString
  }

  test("Message with missing keys should throw AbortException") {
    val content = loadFileAsString("activemq_msg_faulty.json")
    assertThrows[AbortException](FedoraEvent.fromJson(content))
  }

  test("Event not stemming from a relevant object should be ignored") {
    val content = loadFileAsString("activemq_msg_irrelevant.json")
    assert(FedoraEvent.fromJson(content).getObjectType == Other)
  }

  test("Event stemming from a relevant object should be processed") {
    val res = s"""{"eventId":"abcd","eventTimestamp":"1234","eventType":"Create","objectPath":"$externalBaseUrl/record/test-1","objectType":"rico:Record","objectVersion":"0"}"""
    val content = loadFileAsString("activemq_msg_ok.json")
    val fe = FedoraEvent.fromJson(content)
    assert(fe.getObjectType == Record)
    val kafkaMsg = fe.toKafkaMessage("abcd", 1234, fedoraBaseUrl, externalBaseUrl)
    assert(res == kafkaMsg)
  }

  test("The object id should be the last part of the object URL if object not versioned") {
    val content = loadFileAsString("activemq_msg_ok.json")
    assert(Record.recordId(FedoraEvent.fromJson(content).eventObject.id) == "test-1")
  }

  test("The object id should be the last part before `fcr:versions` part of the object URL if object is versioned") {
    val content = loadFileAsString("activemq_msg_versioned.json")
    val fe = FedoraEvent.fromJson(content)
    assert(VersionedRecord.recordId(FedoraEvent.fromJson(content).eventObject.id) == "test-1")
  }

  test("Objects which are blank nodes are not relevant") {
    val content = loadFileAsString("activemq_msg_blanknode.json")
    assert(FedoraEvent.fromJson(content).getObjectType == RecordBlankNode)
  }
}
