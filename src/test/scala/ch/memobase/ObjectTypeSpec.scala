/*
 * Fedora Event Handler
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import org.scalatest.funsuite.AnyFunSuite

class ObjectTypeSpec extends AnyFunSuite {

  val prefix = "https://mb-fed1.memobase.unibas.ch/fcrepo/rest"
  val blankNodeId = "genidd125843b-cbbe-4615-8501-150499947c2e"
  val version = "fcr:versions/344"
  val recordId = "aef-a432-ef_02"
  val instantiationId = "aef-a432-ef_02-1"

  test("the ObjectType constructor should create a RecordSet object if provided a recordSet id") {
    val id = s"$prefix/recordSet/$recordId"
    assert(ObjectType(id) == RecordSet)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).isEmpty)
  }

  test("the ObjectType constructor should create a RecordSetBlankNode object if provided a recordSet blank node id") {
    val id = s"$prefix/recordSet/$recordId#$blankNodeId"
    assert(ObjectType(id) == RecordSetBlankNode)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).isEmpty)
  }

  test("the ObjectType constructor should create a VersionedRecordSet object if provided a versioned recordSet id") {
    val id = s"$prefix/recordSet/$recordId/$version"
    assert(ObjectType(id) == VersionedRecordSet)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).getOrElse("") == version.split("/")(1))
  }

  test("the ObjectType constructor should create a CorporateBody object if provided a corporateBody id") {
    val id = s"$prefix/institution/$recordId"
    assert(ObjectType(id) == CorporateBody)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).isEmpty)
  }

  test("the ObjectType constructor should create a CorporateBodyBlankNode object if provided a corporateBody blank node id") {
    val id = s"$prefix/institution/$recordId#$blankNodeId"
    assert(ObjectType(id) == CorporateBodyBlankNode)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).isEmpty)
  }

  test("the ObjectType constructor should create a VersionedCorporateBody object if provided a versioned corporateBody id") {
    val id = s"$prefix/institution/$recordId/$version"
    assert(ObjectType(id) == VersionedCorporateBody)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).getOrElse("") == version.split("/")(1))
  }

  test("the ObjectType constructor should create a Record object if provided a record id") {
    val id = s"$prefix/record/$recordId"
    assert(ObjectType(id) == Record)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).isEmpty)
  }

  test("the ObjectType constructor should create a RecordBlankNode object if provided a record blank node id") {
    val id = s"$prefix/record/$recordId#$blankNodeId"
    assert(ObjectType(id) == RecordBlankNode)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).isEmpty)
  }

  test("the ObjectType constructor should create a VersionedRecord object if provided a versioned record id") {
    val id = s"$prefix/record/$recordId/$version"
    assert(ObjectType(id) == VersionedRecord)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).getOrElse("") == version.split("/")(1))
  }

  test("the ObjectType constructor should create a PhysicalObject object if provided a physicalObject id") {
    val id = s"$prefix/physical/$instantiationId"
    assert(ObjectType(id) == PhysicalObject)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).isEmpty)
  }

  test("the ObjectType constructor should create a PhysicalObjectBlankNode object if provided a physicalObject blank node id") {
    val id = s"$prefix/physical/$instantiationId#$blankNodeId"
    assert(ObjectType(id) == PhysicalObjectBlankNode)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).isEmpty)
  }

  test("the ObjectType constructor should create a VersionedPhysicalObject object if provided a versioned physicalObject id") {
    val id = s"$prefix/physical/$instantiationId/$version"
    assert(ObjectType(id) == VersionedPhysicalObject)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).getOrElse("") == version.split("/")(1))
  }

  test("the ObjectType constructor should create a DigitalObject object if provided a digitalObject id") {
    val id = s"$prefix/digital/$instantiationId"
    assert(ObjectType(id) == DigitalObject)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).isEmpty)
  }

  test("the ObjectType constructor should create a DigitalObjectBlankNode object if provided a digitalObject blank node id") {
    val id = s"$prefix/digital/$instantiationId#$blankNodeId"
    assert(ObjectType(id) == DigitalObjectBlankNode)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).isEmpty)
  }

  test("the ObjectType constructor should create a VersionedDigitalObject object if provided a versioned digitalObject id") {
    val id = s"$prefix/digital/$instantiationId/$version"
    assert(ObjectType(id) == VersionedDigitalObject)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).getOrElse("") == version.split("/")(1))
  }

  test("the ObjectType constructor should create a DigitalObjectBinary object if provided a digitalObject binary id") {
    val id = s"$prefix/digital/$instantiationId/binary"
    assert(ObjectType(id) == DigitalObjectBinary)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).isEmpty)
  }

  test("the ObjectType constructor should create a VersionedDigitalObjectBinary object if provided a versioned digitalObject binary id") {
    val id = s"$prefix/digital/$instantiationId/binary/$version"
    assert(ObjectType(id) == VersionedDigitalObjectBinary)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).getOrElse("") == version.split("/")(1))
  }

  test("the ObjectType constructor should create a DigitalObjectBinaryMetadata object if provided a digitalObject binary metadata id") {
    val id = s"$prefix/digital/$instantiationId/binary/fcr:metadata"
    assert(ObjectType(id) == DigitalObjectBinaryMetadata)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).isEmpty)
  }

  test("the ObjectType constructor should create a VersionedDigitalObjectBinaryMetadata object if provided a versioned digitalObject binary metadata id") {
    val id = s"$prefix/digital/$instantiationId/binary/fcr:metadata/$version"
    assert(ObjectType(id) == VersionedDigitalObjectBinaryMetadata)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).getOrElse("") == version.split("/")(1))
  }

  test("the ObjectType constructor should create a Poster object if provided a poster id") {
    val id = s"$prefix/digital/$instantiationId/derived"
    assert(ObjectType(id) == Poster)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).isEmpty)
  }

  test("the ObjectType constructor should create a VersionedPoster object if provided a versioned poster id") {
    val id = s"$prefix/digital/$instantiationId/derived/$version"
    assert(ObjectType(id) == VersionedPoster)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).getOrElse("") == version.split("/")(1))
  }

  test("the ObjectType constructor should create a PosterBinary object if provided a poster binary id") {
    val id = s"$prefix/digital/$instantiationId/derived/binary"
    assert(ObjectType(id) == PosterBinary)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).isEmpty)
  }

  test("the ObjectType constructor should create a VersionedPosterBinary object if provided a versioned poster binary id") {
    val id = s"$prefix/digital/$instantiationId/derived/binary/$version"
    assert(ObjectType(id) == VersionedPosterBinary)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).getOrElse("") == version.split("/")(1))
  }

  test("the ObjectType constructor should create a PosterBinaryMetadata object if provided a poster binary metadata id") {
    val id = s"$prefix/digital/$instantiationId/derived/binary/fcr:metadata"
    assert(ObjectType(id) == PosterBinaryMetadata)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).isEmpty)
  }

  test("the ObjectType constructor should create a VersionedPosterBinaryMetadata object if provided a versioned poster binary metadata id") {
    val id = s"$prefix/digital/$instantiationId/derived/binary/fcr:metadata/$version"
    assert(ObjectType(id) == VersionedPosterBinaryMetadata)
    assert(ObjectType(id).recordId(id) == recordId)
    assert(ObjectType(id).version(id).getOrElse("") == version.split("/")(1))
  }

  test("the ObjectType constructor should create an Other object if id can't be recognised") {
    val id = s"$prefix/unrelated/$recordId"
    assert(ObjectType(id) == Other)
    assert(ObjectType(id).recordId(id) == id)
    assert(ObjectType(id).version(id).isEmpty)
  }
}
