/*
 * Fedora Event Handler
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import org.apache.activemq.ActiveMQConnectionFactory
import org.apache.logging.log4j.scala.Logging

import java.net.{URI, URISyntaxException}
import javax.jms._
import scala.util.{Failure, Success, Try}

class ActiveMQClientWrapper(connection: Connection, session: Session, consumer: MessageConsumer) {

  def receive: Try[Message] = Try {
    consumer.receive()
  }

  def close: Try[Unit] = Try {
    consumer.close()
    session.close()
    connection.close()
  }
}

object ActiveMQClientWrapper extends Logging {
  def apply(url: String,
            clientId: String,
            queueName: String,
            connectionRetries: Int,
            connectionRetriesWait: Int): ActiveMQClientWrapper = {
    this (url, clientId, queueName, None, None, connectionRetries, connectionRetriesWait)
  }

  def apply(url: String,
            clientId: String,
            topicName: String,
            subscriberName: String,
            connectionRetries: Int,
            connectionRetriesWait: Int): ActiveMQClientWrapper = {
    this (url, clientId, topicName, Some(subscriberName), None, connectionRetries, connectionRetriesWait)
  }

  def apply(url: String,
            username: String,
            password: String,
            clientId: String,
            queueName: String,
            connectionRetries: Int,
            connectionRetriesWait: Int): ActiveMQClientWrapper = {
    this (url, clientId, queueName, None, Some((username, password)), connectionRetries, connectionRetriesWait)
  }

  def apply(url: String,
            username: String,
            password: String,
            clientId: String,
            topicName: String,
            subscriberName: String,
            connectionRetries: Int,
            connectionRetriesWait: Int): ActiveMQClientWrapper = {
    this (url, clientId, topicName, Some(subscriberName), Some((username, password)), connectionRetries, connectionRetriesWait)
  }

  private def apply(url: String,
                    clientId: String,
                    queueName: String,
                    subscriberName: Option[String],
                    credentials: Option[(String, String)],
                    connectionRetries: Int,
                    connectionRetriesWait: Int): ActiveMQClientWrapper = {
    val stream = if (connectionRetries < 0) {
      Stream.continually[Try[ActiveMQClientWrapper]](Failure(new Exception))
    } else {
      Stream.fill[Try[ActiveMQClientWrapper]](connectionRetries + 1)(Failure(new Exception))
    }
    stream
      .zipWithIndex
      .map(x => {
        connectToFedora(url, clientId, queueName, subscriberName, credentials)
          .recoverWith {
            case ex: JMSException =>
              logger.warn((if (connectionRetries > 0) s"[${x._2 + 1}/${connectionRetries + 1}] " else "") +
                s"Can't connect to Fedora on tcp://$url: ${ex.getMessage}. Retrying in ${connectionRetriesWait}ms")
              if (x._2 < connectionRetries || connectionRetries < 0) {
                Thread.sleep(connectionRetriesWait)
              }
              Failure(ex)
            case ex: URISyntaxException =>
              logger.error(s"URI is malformed: $ex")
              sys.exit(1)
          }
      })
      .collectFirst { case Success(amq) => amq } match {
      case Some(amq) => amq
      case None =>
        logger.error(s"Couldn't connect to Fedora on tcp://$url. Aborting")
        sys.exit(1)
    }
  }

  private def connectToFedora(url: String,
                              clientId: String,
                              queueName: String,
                              subscriberName: Option[String],
                              credentials: Option[(String, String)]): Try[ActiveMQClientWrapper] = Try {
    val uri = new URI(s"tcp://$url")
    val connectionFactory = new ActiveMQConnectionFactory(uri)
    val connection = credentials match {
      case Some((username, password)) => connectionFactory.createConnection(username, password)
      case None => connectionFactory.createConnection()
    }
    connection.setClientID(clientId)
    connection.start()
    val session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE)
    val consumer = if (subscriberName.isDefined) {
      session.createDurableSubscriber(session.createTopic(queueName), subscriberName.get)
    } else {
      session.createConsumer(session.createQueue(queueName))
    }
    new ActiveMQClientWrapper(connection, session, consumer)
  }
}
