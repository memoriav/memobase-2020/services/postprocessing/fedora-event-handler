/*
 * Fedora Event Handler
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import scala.util.matching.Regex

sealed trait ObjectType {
  def matches(objectId: String): Boolean = (idPattern findFirstIn objectId).isDefined

  protected val blankNodeId = raw"""#genid[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}"""

  val idPattern: Regex
  val name: String
  val ignore: Boolean = true
  val recordId: String => String = objectId =>
    idPattern findFirstMatchIn objectId match {
      case Some(m) => m.group(1)
      case None => ""
    }

  val version: String => Option[String] = objectId =>
    idPattern findFirstMatchIn objectId match {
      case Some(m) if m.groupCount > 1 => Some(m.group(2))
      case _ => None
    }
}

//noinspection ScalaStyle
object ObjectType {
  def apply(objectId: String): ObjectType = objectId match {
    case id if RecordSet matches id => RecordSet
    case id if VersionedRecordSet matches id => VersionedRecordSet
    case id if RecordSetBlankNode matches id => RecordSetBlankNode
    case id if CorporateBody matches id => CorporateBody
    case id if VersionedCorporateBody matches id => VersionedCorporateBody
    case id if CorporateBodyBlankNode matches id => CorporateBodyBlankNode
    case id if Record matches id => Record
    case id if VersionedRecord matches id => VersionedRecord
    case id if RecordBlankNode matches id => RecordBlankNode
    case id if PhysicalObject matches id => PhysicalObject
    case id if VersionedPhysicalObject matches id => VersionedPhysicalObject
    case id if PhysicalObjectBlankNode matches id => PhysicalObjectBlankNode
    case id if DigitalObject matches id => DigitalObject
    case id if VersionedDigitalObject matches id => VersionedDigitalObject
    case id if DigitalObjectBlankNode matches id => DigitalObjectBlankNode
    case id if DigitalObjectBinary matches id => DigitalObjectBinary
    case id if VersionedDigitalObjectBinary matches id => VersionedDigitalObjectBinary
    case id if DigitalObjectBinaryMetadata matches id => DigitalObjectBinaryMetadata
    case id if VersionedDigitalObjectBinaryMetadata matches id => VersionedDigitalObjectBinaryMetadata
    case id if Poster matches id => Poster
    case id if VersionedPoster matches id => VersionedPoster
    case id if PosterBinary matches id => PosterBinary
    case id if VersionedPosterBinary matches id => VersionedPosterBinary
    case id if PosterBinaryMetadata matches id => PosterBinaryMetadata
    case id if VersionedPosterBinaryMetadata matches id => VersionedPosterBinaryMetadata
    case _ => Other
  }
}

case object RecordSet extends ObjectType {
  override val idPattern: Regex = ".*/recordSet/([^/#]+)$".r
  override val name: String = "recordSet"
  override val ignore: Boolean = false
}

case object VersionedRecordSet extends ObjectType {
  override val idPattern: Regex = ".*/recordSet/([^/#]+)/fcr:versions/?(.*)$".r
  override val name: String = "versionedRecordSet"
}

case object RecordSetBlankNode extends ObjectType {
  override val idPattern: Regex = raw""".*/recordSet/([^/#]+)$blankNodeId$$""".r
  override val name: String = "recordSetBlankNode"
}

case object CorporateBody extends ObjectType {
  override val idPattern: Regex = ".*/institution/([^/#]+)$".r
  override val name: String = "corporateBody"
  override val ignore: Boolean = false
}

case object VersionedCorporateBody extends ObjectType {
  override val idPattern: Regex = ".*/institution/([^/#]+)/fcr:versions/?(.*)$".r
  override val name: String = "versionedCorporateBody"
}

case object CorporateBodyBlankNode extends ObjectType {
  override val idPattern: Regex = raw""".*/institution/([^/#]+)$blankNodeId$$""".r
  override val name: String = "corporateBodyBlankNode"
}

case object Record extends ObjectType {
  override val idPattern: Regex = ".*/record/([^/#]+)$".r
  override val name: String = "record"
  override val ignore: Boolean = false
}

case object VersionedRecord extends ObjectType {
  override val idPattern: Regex = ".*/record/([^/#]+)/fcr:versions/?(.*)$".r
  override val name: String = "versionedRecord"
}

case object RecordBlankNode extends ObjectType {
  override val idPattern: Regex = raw""".*/record/([^/#]+)$blankNodeId$$""".r
  override val name: String = "recordBlankNode"
}

case object PhysicalObject extends ObjectType {
  override val idPattern: Regex = ".*/physical/([^/#]+)-\\d+$".r
  override val name: String = "physicalObject"
}

case object VersionedPhysicalObject extends ObjectType {
  override val idPattern: Regex = ".*/physical/([^/#]+)-\\d+/fcr:versions/?(.*)$".r
  override val name: String = "versionedPhysicalObject"
}

case object PhysicalObjectBlankNode extends ObjectType {
  override val idPattern: Regex = raw""".*/physical/([^#]+)-\d+$blankNodeId$$""".r
  override val name: String = "physicalObjectBlankNode"
}

case object DigitalObject extends ObjectType {
  override val idPattern: Regex = ".*/digital/([^/#]+)-\\d+$".r
  override val name: String = "digitalObject"
}

case object VersionedDigitalObject extends ObjectType {
  override val idPattern: Regex = ".*/digital/([^/#]+)-\\d+/fcr:versions/?(.*)$".r
  override val name: String = "versionedDigitalObject"
}

case object DigitalObjectBlankNode extends ObjectType {
  override val idPattern: Regex = raw""".*/digital/([^/#]+)-\d+$blankNodeId$$""".r
  override val name: String = "digitalObjectBlankNode"
}

case object DigitalObjectBinary extends ObjectType {
  override val idPattern: Regex = ".*/digital/([^/#]+)-\\d+/binary$".r
  override val name: String = "digitalObjectBinary"
}

case object VersionedDigitalObjectBinary extends ObjectType {
  override val idPattern: Regex = ".*/digital/([^/#]+)-\\d+/binary/fcr:versions/?(.*)$".r
  override val name: String = "versionedDigitalObjectBinary"
}

case object DigitalObjectBinaryMetadata extends ObjectType {
  override val idPattern: Regex = ".*/digital/([^/#]+)-\\d+/binary/fcr:metadata$".r
  override val name: String = "digitalObjectBinaryMetadata"
}

case object VersionedDigitalObjectBinaryMetadata extends ObjectType {
  override val idPattern: Regex = ".*/digital/([^/#]+)-\\d+/binary/fcr:metadata/fcr:versions/?(.*)$".r
  override val name: String = "versionedDigitalObjectBinaryMetadata"
}

case object Poster extends ObjectType {
  override val idPattern: Regex = ".*/digital/([^/#]+)-\\d+/derived$".r
  override val name: String = "poster"
}

case object VersionedPoster extends ObjectType {
  override val idPattern: Regex = ".*/digital/([^/#]+)-\\d+/derived/fcr:versions/?(.*)$".r
  override val name: String = "versionedPoster"
}

case object PosterBinary extends ObjectType {
  override val idPattern: Regex = ".*/digital/([^/#]+)-\\d+/derived/binary$".r
  override val name: String = "posterBinary"
}

case object VersionedPosterBinary extends ObjectType {
  override val idPattern: Regex = ".*/digital/([^/#]+)-\\d+/derived/binary/fcr:versions/?(.*)$".r
  override val name: String = "versionedPosterBinary"
}

case object PosterBinaryMetadata extends ObjectType {
  override val idPattern: Regex = ".*/digital/([^/#]+)-\\d+/derived/binary/fcr:metadata$".r
  override val name: String = "posterBinaryMetadata"
}

case object VersionedPosterBinaryMetadata extends ObjectType {
  override val idPattern: Regex = ".*/digital/([^/#]+)-\\d+/derived/binary/fcr:metadata/fcr:versions/?(.*)$".r
  override val name: String = "versionedPosterBinaryMetadata"
}

case object Other extends ObjectType {
  override val idPattern: Regex = "^(.*)$".r
  override val name: String = "other"
}
