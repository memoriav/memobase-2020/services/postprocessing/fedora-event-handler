/*
 * Fedora Event Handler
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import org.apache.logging.log4j.scala.Logging

case class FedoraEvent(id: String,
                       @upickle.implicits.key("type") eventType: List[String],
                       published: String,
                       @upickle.implicits.key("object") eventObject: FedoraObject) extends Logging {

  def getObjectType: ObjectType = ObjectType(eventObject.id)

  def toKafkaMessage(msgId: String, msgTimestamp: Long, fedoraBaseUrl: String, externalBaseUrl: String): String = {
    val objectPath = eventObject.id.replaceAllLiterally(fedoraBaseUrl, externalBaseUrl)
    val version = getVersion
    val jsonResult = ujson.Obj(
      "eventId" -> msgId,
      "eventTimestamp" -> msgTimestamp,
      "eventType" -> createEventType,
      "objectPath" -> objectPath,
      "objectType" -> s"rico:${getObjectType.name.charAt(0).toUpper}${getObjectType.name.substring(1)}",
      "objectVersion" -> version
    )
    upickle.default.write(jsonResult)
  }

  private def getVersion: String =
    ".*/fcr:versions/([0-9]{14})$".r
      .findFirstMatchIn(eventObject.id)
      .flatMap(m => Some(m.group(1)))
      .getOrElse("0")

  private def createEventType: String = eventType match {
    case eT if eT.contains("Create") => "Create"
    case eT if eT.contains("Delete") => "Delete"
    case eT if eT.contains("Update") => "Update"
  }
}

object FedoraEvent {
  implicit val fedoraObjectRW: upickle.default.ReadWriter[FedoraObject] = upickle.default.macroRW[FedoraObject]
  implicit val fedoraEventRW: upickle.default.ReadWriter[FedoraEvent] = upickle.default.macroRW[FedoraEvent]

  def fromJson(text: String): FedoraEvent = {
    upickle.default.read[FedoraEvent](text)
  }
}

case class FedoraObject(@upickle.implicits.key("type") objectType: List[String],
                        id: String,
                        isPartOf: String)
