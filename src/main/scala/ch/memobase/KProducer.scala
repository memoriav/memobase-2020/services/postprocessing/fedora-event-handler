/*
 * Fedora Event Handler
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.Report
import monix.eval.Task
import monix.execution.Cancelable
import monix.execution.Scheduler.Implicits.global
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.header.Header
import org.apache.logging.log4j.scala.Logging

import java.time.Duration
import java.util.Properties
import scala.collection.JavaConverters._
import scala.concurrent.duration.DurationInt

//noinspection ScalaStyle
class KProducer(props: Properties, topic: String, reportTopic: String) extends Logging {
  val producer = new KafkaProducer[String, String](props)

  def send(key: String, message: String, delayInSec: Int): Cancelable = {
    logger.debug(s"Scheduling message with key $key with a delay of ${delayInSec}s")
    val task = Task(
      producer.send(new ProducerRecord[String, String](topic, null, key, message, createHeaders(key)))
    )
      .delayExecution(delayInSec.second)

    task.runAsync {
      case Right(_) =>
        logger.debug(s"Message with key $key sent after delay of ${delayInSec}s")
      case Left(ex) =>
        logger.warn(s"Errors occurred sending the message with key $key: ${ex.getMessage}")
    }
  }

  def sendReport(report: Report, delayInSec: Int): Cancelable = {
    logger.debug(s"Scheduling report with id ${report.id} with a delay of ${delayInSec}s")
    val record = new ProducerRecord[String, String](reportTopic, null, report.id, report.toString, createHeaders(report.id))
    val task = Task(producer.send(record))
      .delayExecution(delayInSec.second)

    task.runAsync {
      case Right(_) =>
        logger.debug(s"Report with id ${report.id} sent after delay of ${delayInSec}s")
      case Left(ex) =>
        logger.warn(s"Errors occured sending the report with key ${report.id}: ${ex.getMessage}")
    }
  }

  def close(timeout: Duration): Unit = {
    producer.close(timeout)
  }

  private def createHeaders(recordId: String): java.lang.Iterable[Header] = {
    val idPattern = "https://memobase.ch/([^/]+)/([^#/]+).*".r
    val (institutionId, recordSetId) = idPattern.findFirstMatchIn(recordId) match {
      case Some(m) => m.groupCount match {
        case gc if gc >= 2 =>
          m.group(1) match {
            case "record" =>
              m.group(2).split("-", 3).toList match {
                case institution :: recordSet :: _ => (institution, s"$institution-$recordSet")
                case _ =>
                  logger.warn(s"Can't extract institutionId and recordSetId from uri $recordId")
                  ("PLACEHOLDER", "PLACEHOLDER")
              }
            case "recordSet" =>
              m.group(2).split("-", 2).toList match {
                case institution :: recordSet :: _ => (institution, s"$institution-$recordSet")
                case _ =>
                  logger.warn(s"Can't extract institutionId and recordSetId from uri $recordId")
                  ("PLACEHOLDER", "PLACEHOLDER")
              }
            case "institution" =>
              (m.group(2), "PLACEHOLDER")
            case r =>
              logger.warn(s"$r is unknown resource type")
              ("PLACEHOLDER", "PLACEHOLDER")
          }
        case _ =>
          logger.warn(s"Can't extract institutionId and recordSetId from uri $recordId")
          ("PLACEHOLDER", "PLACEHOLDER")
      }
      case None =>
        logger.warn(s"Can't extract institutionId and recordSetId from uri $recordId")
        ("PLACEHOLDER", "PLACEHOLDER")
    }
    List(
      new Header {
        override def key(): String = "recordSetId"

        override def value(): Array[Byte] = recordSetId.getBytes
      },
      new Header {
        override def key(): String = "institutionId"

        override def value(): Array[Byte] = institutionId.getBytes
      }
    ).asJava
  }
}

object KProducer {
  def apply[K, V](props: Properties, topic: String, reportTopic: String): KProducer = new KProducer(props, topic, reportTopic)
}
