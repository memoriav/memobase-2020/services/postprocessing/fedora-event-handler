/*
 * Fedora Event Handler
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.{ProcessingIgnore, ProcessingSuccess, Report}
import org.apache.logging.log4j.scala.Logging
import ch.memobase.settings.SettingsLoader
import upickle.core.AbortException

import java.time.Duration
import javax.jms.{JMSException, TextMessage}
import scala.collection.JavaConverters._
import scala.util.{Failure, Success, Try}


object App extends scala.App with Logging with FedoraClient {

  def createRecordId(objectType: ObjectType, fedoraEvent: FedoraEvent): String =
    objectType match {
      case RecordSet | RecordSetBlankNode | VersionedRecordSet =>
        s"https://memobase.ch/recordSet/${objectType.recordId(fedoraEvent.eventObject.id)}"
      case CorporateBody | CorporateBodyBlankNode | VersionedCorporateBody =>
        s"https://memobase.ch/institution/${objectType.recordId(fedoraEvent.eventObject.id)}"
      case Other =>
        fedoraEvent.eventObject.id
      case _ =>
        s"https://memobase.ch/record/${objectType.recordId(fedoraEvent.eventObject.id)}"
    }

  val settings = new SettingsLoader(List(
    "activeMQHost",
    "activeMQPort",
    "activeMQPublishingMode",
    "activeMQQueueOrTopicName",
    "fedoraBaseUrl",
    "fedoraConnectionRetries",
    "fedoraConnectionRetriesWait",
    "externalBaseUrl",
    "recordProcessDelaySec",
    "recordSetProcessDelaySec",
    "corporateBodyProcessDelaySec",
    "reportIgnore"
  ).asJava,
    "app.yml",
    true,
    false,
    false,
    false)


  val activeMQHost = settings.getAppSettings.getProperty("activeMQHost")
  val activeMQPort = settings.getAppSettings.getProperty("activeMQPort").toInt
  val activeMQPublishingMode = settings.getAppSettings.getProperty("activeMQPublishingMode")
  val activeMQQueueOrTopicName = settings.getAppSettings.getProperty("activeMQQueueOrTopicName")
  val fedoraBaseUrl = settings.getAppSettings.getProperty("fedoraBaseUrl").replaceAll("/$", "")
  val fedoraConnectionRetries = settings.getAppSettings.getProperty("fedoraConnectionRetries").toInt
  val fedoraConnectionRetriesWait = settings.getAppSettings.getProperty("fedoraConnectionRetriesWait").toInt
  val externalBaseUrl = settings.getAppSettings.getProperty("externalBaseUrl").replaceAll("/$", "")
  val recordProcessDelaySec = settings.getAppSettings.getProperty("recordProcessDelaySec").toInt
  val recordSetProcessDelaySec = settings.getAppSettings.getProperty("recordSetProcessDelaySec").toInt
  val corporateBodyProcessDelaySec = settings.getAppSettings.getProperty("corporateBodyProcessDelaySec").toInt
  val reportIgnore = Try(settings.getAppSettings.getProperty("reportIgnore").toBoolean).getOrElse(false)
  val shutdownGracePeriodMs = 10000

  val kafkaProps = settings.getKafkaProducerSettings

  var amh = connect(activeMQPublishingMode,
    s"$activeMQHost:$activeMQPort",
    activeMQQueueOrTopicName,
    fedoraConnectionRetries,
    fedoraConnectionRetriesWait)
  logger.info(s"Connecting to Kafka on ${kafkaProps.getProperty("bootstrap.servers")}")
  val kp = KProducer(kafkaProps, settings.getOutputTopic, settings.getProcessReportTopic)

  logger.info("Start reading from ActiveMQ")

  while (true) {
    amh.receive match {
      case Success(msg: TextMessage) =>
        logger.debug(s"Receiving message from Fedora: ${msg.getText}")
        try {
          val fe = FedoraEvent.fromJson(msg.getText)
          val objectType = fe.getObjectType
          if (!objectType.ignore) {
            val jsonString = fe.toKafkaMessage(msg.getJMSMessageID, msg.getJMSTimestamp, fedoraBaseUrl, externalBaseUrl)
            val delay = objectType match {
              case Record => recordProcessDelaySec
              case RecordSet => recordSetProcessDelaySec
              case CorporateBody => corporateBodyProcessDelaySec
              case _ => 0
            }
            logger.debug(s"Scheduling message ${msg.getJMSMessageID} with content $jsonString")
            kp.send(createRecordId(objectType, fe), jsonString, delay)
            val report = Report(
              createRecordId(objectType, fe),
              ProcessingSuccess,
              s"event${if (fe.eventType.length > 1) "s" else ""} ${fe.eventType.mkString(", ")} " +
                s"for ${fe.getObjectType.name} resource with uri ${fe.eventObject.id} created"
            )
            kp.sendReport(report, delay)
          } else {
            logger.debug(s"Ignoring event ${msg.getJMSMessageID} for ${objectType.name} because it is not relevant")
            if (reportIgnore) {
              val report = Report(
                createRecordId(objectType, fe),
                ProcessingIgnore,
                s"since events for ${fe.getObjectType.name} resources are ignored, resource ${fe.eventObject.id} is skipped"
              )
              kp.sendReport(report, 0)
            }
          }

        } catch {
          case e: JMSException =>
            logger.error(s"Parsing exception: ${e.getLinkedException.getMessage}")
          case e: AbortException =>
            logger.error(s"Parsing exception: ${e.clue}")
        }
      case Success(msg) =>
        logger.warn(s"Fedora sent a message which is not a `TextMessage` (message id: ${msg.getJMSMessageID})")
      case Failure(ex) =>
        logger.warn(s"Lost connection to Fedora (${ex.getMessage}). Retrying to connect")
        amh = connect(activeMQPublishingMode,
          s"$activeMQHost:$activeMQPort",
          activeMQQueueOrTopicName,
          -1,
          fedoraConnectionRetriesWait,
          reconnect = true)
    }
  }
  kp.close(Duration.ofMillis(shutdownGracePeriodMs))
  amh.close

}

trait FedoraClient extends Logging {
  def connect(publishingMode: String,
              url: String,
              queueOrTopicName: String,
              connectionRetries: Int,
              connectionRetriesWait: Int,
              reconnect: Boolean = false): ActiveMQClientWrapper = {
    logger.info(s"${if (reconnect) "Rec" else "C"}onnecting to ActiveMQ on $url")
    publishingMode match {
      case "queue" =>
        ActiveMQClientWrapper(url, "fedora-event-handler", queueOrTopicName, connectionRetries, connectionRetriesWait)
      case "topic" =>
        ActiveMQClientWrapper(url, "fedora-event-handler", queueOrTopicName, "event-catcher", connectionRetries, connectionRetriesWait)
      case _ =>
        logger.error("ACTIVEMQ_PUBLISHING_MODE must be `queue` or `topic`!")
        scala.sys.exit(1)
    }
  }
}