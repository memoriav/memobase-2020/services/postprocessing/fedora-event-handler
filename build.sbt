import Dependencies._

ThisBuild / scalaVersion := "2.12.11"
ThisBuild / organization := "ch.memobase"
ThisBuild / organizationName := "Memobase"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") {
    Some(tag)
  } else {
    None
  }
}

lazy val root = (project in file("."))
  .settings(
    name := "Fedora Event Handler",
    assembly / assemblyJarName := "app.jar",
    assembly / test := {},
    assembly / mainClass := Some("ch.memobase.App"),
    assembly / assemblyMergeStrategy := {
      case "log4j.properties" => MergeStrategy.first
      case other if other.contains("module-info.class") => MergeStrategy.discard
      case "log4j2.xml" => MergeStrategy.first
      case x =>
        val oldStrategy = (assembly / assemblyMergeStrategy).value
        oldStrategy(x)
    },
    resolvers ++= Seq(
      "Memobase Libraries" at "https://gitlab.switch.ch/api/v4/projects/1324/packages/maven"
    ),
    libraryDependencies ++= Seq(
      activemqClient,
      kafkaClients,
      log4jApi,
      log4jCore,
      log4jSlf4j,
      log4jScala,
      memobaseServiceUtils excludeAll (
        ExclusionRule(organization = "org.slf4j", name = "slf4j-api"),
        ExclusionRule(organization = "org.slf4j", name = "slf4j-log4j12"),
        ExclusionRule(organization = "org.slf4j", name = "jcl-over-slf4j"),
      ),
      monixEval,
      uPickle,
      scalaTest % Test
    ))

